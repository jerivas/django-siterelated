from setuptools import setup, find_packages

import siterelated

setup(
    name="django-siterelated",
    version=siterelated.__version__,
    description=siterelated.__doc__,
    packages=find_packages(),
    url="https://gitlab.com/jerivas/django-siterelated/",
    author="jerivas",
    long_description=open("README.md").read(),
    include_package_data=True,
    license="Creative Commons Attribution 3.0 Unported",
)
