from __future__ import unicode_literals

from django import forms

from .utils import current_site_id


class HiddenSiteAdminMixin(object):
    """
    Hide the `site` field in normal and inline admins.
    Subclasses MUST include "site" in their fields or fieldsets.
    This is required for "unique_together" checks that involve "site".
    """

    class Media:
        """
        Include the CSS that will hide the little plus sign for `site`.
        """
        css = {
            "all": ("admin/siterelated.css",)
        }

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        Hide the `site` field and add the current site as initial value.
        """
        if db_field.name == "site":
            kwargs["widget"] = forms.HiddenInput()
            kwargs["initial"] = current_site_id()
        return super(HiddenSiteAdminMixin, self).formfield_for_foreignkey(
            db_field, request, **kwargs)
