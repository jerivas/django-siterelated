from __future__ import unicode_literals

import os
import threading
from contextlib import contextmanager

from django.contrib.sites.models import Site
from django.core.cache import cache

from django.conf import settings
from .request import current_request


def current_site_id():
    """
    Responsible for determining the current ``Site`` instance to use
    when retrieving data for any ``SiteRelated`` models. If we're inside an
    override_current_site_id context manager, return the overriding site ID.
    Otherwise, try to determine the site using the following methods in order:
      - ``site_id`` in session. Used in the admin so that admin users
        can switch sites and stay on the same domain for the admin.
      - The id of the Site object corresponding to the hostname in the current
        request. This result is cached.
      - ``DJANGO_SITE_ID`` environment variable, so management
        commands or anything else outside of a request can specify a
        site.
      - ``SITE_ID`` setting.
    If a current request exists and the current site is not overridden, the
    site ID is stored on the request object to speed up subsequent calls.
    """

    if hasattr(override_current_site_id.thread_local, "site_id"):
        return override_current_site_id.thread_local.site_id

    request = current_request()
    site_id = getattr(request, "site_id", None)
    if request and not site_id:
        site_id = request.session.get("site_id", None)
        if not site_id:
            domain = request.get_host().lower()
            cache_key = "%s.site_id.%s" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, domain)
            site_id = cache.get(cache_key)
            if not site_id:
                try:
                    site = Site.objects.get(domain__iexact=domain)
                except Site.DoesNotExist:
                    pass
                else:
                    site_id = site.id
                    cache.set(cache_key, site_id)
    if not site_id:
        site_id = os.environ.get("DJANGO_SITE_ID", settings.SITE_ID)
    if request and site_id and not getattr(settings, "TESTING", False):
        request.site_id = site_id
    return site_id


@contextmanager
def override_current_site_id(site_id):
    """
    Context manager that overrides the current site id for code executed
    within it. Used to access SiteRelated objects outside the current site.
    """
    override_current_site_id.thread_local.site_id = site_id
    yield
    del override_current_site_id.thread_local.site_id


override_current_site_id.thread_local = threading.local()
