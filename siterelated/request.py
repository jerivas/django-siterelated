from __future__ import unicode_literals

import threading

# Middleware mixin for Django 1.10
try:
    from django.utils.deprecation import MiddlewareMixin
except ImportError:
    class MiddlewareMixin(object):
        pass


_thread_local = threading.local()


def current_request():
    """
    Retrieves the request from the current thread.
    """
    return getattr(_thread_local, "request", None)


class CurrentRequestMiddleware(MiddlewareMixin):
    """
    Stores the request in the current thread for global access.
    """

    def process_request(self, request):
        _thread_local.request = request
