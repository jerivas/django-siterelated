from __future__ import unicode_literals

from django.apps import AppConfig


class SiteRelatedConfig(AppConfig):
    name = 'siterelated'
