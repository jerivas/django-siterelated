"""django-siterelated enhances Django's Site framework.
"""

__version__ = '0.2.0'

default_app_config = 'siterelated.apps.SiteRelatedConfig'
